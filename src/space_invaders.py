import pygame
import visual
import collisions as co
import state as st
import moves as mv
from config import GAME_FRAME as game_frame


pygame.init()


def main():
    game_state = st.GameState()
    input_state = st.InputState()
    while input_state.run:
        input_state.update()
        mv.moves(input_state, game_state)
        co.collisions(game_state)
        visual.draw_game(game_frame, game_state)


if __name__ == "__main__":
    main()
