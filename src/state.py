import pygame
import config
import entities as ent
from config import SPRITES as sprites

keybindings = config.get_bindings()


class GameState:
    def __init__(self):
        self.entities = ent.create_entities(sprites)
        self.score = 0
        self.game_over = False
        self.over_sound_not_played = True


class InputState:
    def __init__(self):
        self.run = True
        self.left = False
        self.right = False
        self.fire = False

    def update(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.run = False
            if event.type == pygame.KEYDOWN:
                try:
                    setattr(self, keybindings[event.key], True)
                except KeyError:
                    pass
            if event.type == pygame.KEYUP:
                try:
                    setattr(self, keybindings[event.key], False)
                except KeyError:
                    pass
