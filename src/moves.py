import entities as ent
from config import MISSILE_SPRITE as ms, missiles_sound


def change_invaders_positions(invaders):
    for invader in invaders:
        invader.change_position()


def fire(input_state, game_state):
    if input_state.fire:
        game_state.entities.missiles.append(
            ent.Missile(ms, game_state.entities.player)
        )
        missiles_sound.play()
        input_state.fire = False


def change_missiles_positions(game_state):
    for missile in game_state.entities.missiles:
        missile.change_position()


def moves(input_state, game_state):
    game_state.entities.player.change_position(input_state)
    change_invaders_positions(game_state.entities.invaders)
    fire(input_state, game_state)
    change_missiles_positions(game_state)
