import csv
import pygame
import visual

pygame.mixer.init()


def chosen_config():
    user_choices = {}
    with open("config.txt", encoding="utf-8") as config:
        config = csv.DictReader(config, delimiter=":")
        for row in config:
            user_choices[row["CONFIG"]] = row["USER_CHOICE"]
    return user_choices


configs = chosen_config()
GENERAL_PATH = "assets/"
BACKGROUND_PATH = GENERAL_PATH + "backgrounds/"
FONT_PATH = GENERAL_PATH + "fonts/"
SPRITES_PATH = GENERAL_PATH + "sprites/"
SOUNDS_PATH = GENERAL_PATH + "sounds/"

BACKGROUND = pygame.image.load(BACKGROUND_PATH + configs["background_img"])
TITLE = configs["game_title"]
FONT = FONT_PATH + configs["font"]
PLAYER_SPRITE = pygame.image.load(SPRITES_PATH + configs["player_img"])
INVADER_SPRITE = pygame.image.load(SPRITES_PATH + configs["invaders_img"])
MISSILE_SPRITE = pygame.image.load(SPRITES_PATH + configs["missiles_img"])
COLLISION_SOUND_PATH = SOUNDS_PATH + configs["collision_sound"]
MISSILES_SOUND_PATH = SOUNDS_PATH + configs["missiles_sound"]
PLAYER_SPEED = int(configs["player_speed"])
INVADERS_LATERAL_SPEED = int(configs["invaders_lateral_speed"])
INVADERS_FRONT_SPEED = int(configs["invaders_front_speed"])
NB_INVADERS = int(configs["nb_invaders"])

SCREEN_DIMENSION = BACKGROUND.get_width(), BACKGROUND.get_height()
SCREEN = visual.screen(SCREEN_DIMENSION, TITLE)
GAME_FRAME = SCREEN_DIMENSION, SCREEN, BACKGROUND, FONT
SPRITES = PLAYER_SPRITE, INVADER_SPRITE, MISSILE_SPRITE


class Sound:
    def __init__(self, wav_file):
        self.sound = pygame.mixer.Sound(wav_file)

    def play(self):
        self.sound.play()


collision_sound = Sound(COLLISION_SOUND_PATH)
missiles_sound = Sound(MISSILES_SOUND_PATH)


def chosen_keys():
    key_bindings = {}
    with open("keybindings.txt", encoding="utf-8") as keys:
        keys = csv.DictReader(keys, delimiter=":")
        for row in keys:
            key_bindings[row["MOVE"]] = row["CHOSEN_KEY"]
    return key_bindings


def bindable_keys():
    config_keys = {
        "space": pygame.K_SPACE,
        "up": pygame.K_UP,
        "down": pygame.K_DOWN,
        "right": pygame.K_RIGHT,
        "left": pygame.K_LEFT,
        "0": pygame.K_0,
        "1": pygame.K_1,
        "2": pygame.K_2,
        "3": pygame.K_3,
        "4": pygame.K_4,
        "5": pygame.K_5,
        "6": pygame.K_6,
        "7": pygame.K_7,
        "8": pygame.K_8,
        "9": pygame.K_9,
        "a": pygame.K_a,
        "b": pygame.K_b,
        "c": pygame.K_c,
        "d": pygame.K_d,
        "e": pygame.K_e,
        "f": pygame.K_f,
        "g": pygame.K_g,
        "h": pygame.K_h,
        "i": pygame.K_i,
        "j": pygame.K_j,
        "k": pygame.K_k,
        "l": pygame.K_l,
        "m": pygame.K_m,
        "n": pygame.K_n,
        "o": pygame.K_o,
        "p": pygame.K_p,
        "q": pygame.K_q,
        "r": pygame.K_r,
        "s": pygame.K_s,
        "t": pygame.K_t,
        "u": pygame.K_u,
        "v": pygame.K_v,
        "w": pygame.K_w,
        "x": pygame.K_x,
        "y": pygame.K_y,
        "z": pygame.K_z,
        ".": pygame.K_PERIOD,
        ",": pygame.K_COMMA,
        ":": pygame.K_COLON,
        ";": pygame.K_SEMICOLON,
        "?": pygame.K_QUESTION,
        "!": pygame.K_EXCLAIM,
        "/": pygame.K_SLASH,
    }
    return config_keys


def fusion_dic(dic1, dic2):
    dic3 = {}
    for k, v in dic1.items():
        if v in dic2:
            dic3[dic2[v]] = k
    return dic3


def get_bindings():
    return fusion_dic(chosen_keys(), bindable_keys())
