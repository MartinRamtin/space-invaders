import pygame


def screen(screen_dimension, title):
    x, y = screen_dimension[0], screen_dimension[1]
    pygame.display.set_caption(title)
    return pygame.display.set_mode((x, y))


def background(screen, background):
    screen.blit(background, (0, 0))


def draw_sprite(screen, entity):
    x, y = entity.position[0][0], entity.position[1][0]
    screen.blit(entity.sprite, (x, y))


def define_font(path, size):
    return pygame.font.Font(path, size)


def score(screen, font_path, score_value):
    font = define_font(font_path, 50)
    score_value = font.render(f"Score : {score_value}", True, (0, 255, 0))
    screen.blit(score_value, (30, 20))


def game_over_text(screen_dimension, screen, font_path):
    font = define_font(font_path, 128)
    font_x, font_y = font.size("GAME OVER")
    screen_x, screen_y = screen_dimension
    finish_x = (screen_x - font_x) // 2
    finish_y = (screen_y - font_y) // 2
    game_over = font.render("GAME OVER", True, (255, 0, 0))
    screen.blit(game_over, (finish_x, finish_y))


def draw_game(game_frame, game_state):
    screen_dimension, screen, bground, font_path = game_frame
    if game_state.game_over is True:
        background(screen, bground)
        game_state.entities._replace(invaders=[])
        score(screen, font_path, game_state.score)
        game_over_text(screen_dimension, screen, font_path)
    else:
        background(screen, bground)
        score(screen, font_path, game_state.score)
        draw_sprite(screen, game_state.entities.player)
        for invader in game_state.entities.invaders:
            draw_sprite(screen, invader)
        for missile in game_state.entities.missiles:
            draw_sprite(screen, missile)
    pygame.display.update()
