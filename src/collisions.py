import entities as ent
from config import collision_sound


def is_collision(attacker, target):
    (a_x1, a_x2), (a_y1, a_y2) = attacker.position
    (t_x1, t_x2), (t_y1, t_y2) = target.position
    return (t_x1 <= a_x1 <= t_x2 or t_x1 <= a_x2 <= t_x2) and (
        t_y1 <= a_y1 <= t_y2 or t_y1 <= a_y2 <= t_y2
    )


def collisions(game_state):
    for i, invader in enumerate(game_state.entities.invaders):
        if is_collision(invader, game_state.entities.player):
            game_state.game_over = True
            if game_state.over_sound_not_played:
                collision_sound.play()
                game_state.over_sound_not_played = False
        for missile in game_state.entities.missiles:
            if is_collision(missile, invader):
                game_state.entities.missiles.remove(missile)
                invader = ent.Invader(invader.sprite)
                game_state.entities.invaders[i] = invader
                game_state.score += 1
                collision_sound.play()
