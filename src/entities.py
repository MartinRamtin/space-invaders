from random import randint as rd, choice as ch
from collections import namedtuple
from config import (
    SCREEN_DIMENSION,
    PLAYER_SPEED,
    INVADERS_LATERAL_SPEED as LATERAL_SPEED,
    INVADERS_FRONT_SPEED as FRONT_SPEED,
    NB_INVADERS,
)

scr_w, scr_h = SCREEN_DIMENSION


class Entity:
    def __init__(self, sprite):
        self.sprite = sprite
        self.position = (0, 0), (0, 0)
        self.move = 0
        self.create()

    def create(self):
        pass


class Player(Entity):
    def create(self):
        sp_w, sp_h = self.sprite.get_width(), self.sprite.get_height()
        x1, y1 = scr_w // 2 - sp_w // 2, 0
        y1 = (scr_h - scr_h / 6) - (sp_h / 2)
        self.position = (x1, x1 + sp_w), (y1, y1 + sp_h)
        self.move = 0

    def direction(self, input_state):
        left, right = input_state.left, input_state.right
        if left and not right:
            self.move = -PLAYER_SPEED
        if right and not left:
            self.move = PLAYER_SPEED
        if not left and not right:
            self.move = 0

    def boundaries(self, pl_w):
        x1 = self.position[0][0] + self.move
        right_boundary = SCREEN_DIMENSION[0] - pl_w
        if x1 <= 0:
            x1 = 0
        elif x1 >= right_boundary:
            x1 = right_boundary
        return x1

    def change_position(self, input_state):
        self.direction(input_state)
        player_width = self.sprite.get_width()
        x1 = self.boundaries(player_width)
        self.position = (x1, x1 + player_width), self.position[1]


class Invader(Entity):
    def create(self):
        sp_w, sp_h = self.sprite.get_width(), self.sprite.get_height()
        x1 = rd(0, scr_w - sp_w)
        y1 = rd(0, scr_h - 4 * (scr_h / 6) - (sp_h / 2))
        self.position = (x1, x1 + sp_w), (y1, y1 + sp_h)
        self.move = ch([-LATERAL_SPEED, LATERAL_SPEED])

    def change_position(self):
        scr_w, _ = SCREEN_DIMENSION
        boundary = scr_w - self.sprite.get_width()
        sp_w, sp_h = self.sprite.get_width(), self.sprite.get_height()
        (x1, _), (y1, _) = self.position
        move = self.move
        if x1 <= 0:
            y1 += FRONT_SPEED
            move = -move
        if x1 >= boundary:
            y1 += FRONT_SPEED
            move = -move
        x1 += move
        position = (x1, x1 + sp_w), (y1, y1 + sp_h)
        self.position = position
        self.move = move


class Missile(Entity):
    def __init__(self, sprite, player):
        self.player = player
        super().__init__(sprite)

    def create(self):
        mis_sp_w, mis_sp_h = (
            self.sprite.get_width(),
            self.sprite.get_height(),
        )
        pla_sp_w, _ = (
            self.player.sprite.get_width(),
            self.player.sprite.get_height(),
        )
        (pla_x1, _), (pla_y1, _) = self.player.position
        mis_x1 = pla_x1 + (pla_sp_w - mis_sp_w) / 2
        mis_x2 = mis_x1 + mis_sp_w
        mis_y1 = pla_y1
        mis_y2 = mis_y1 + mis_sp_h
        self.position = (mis_x1, mis_x2), (mis_y1, mis_y2)

    def change_position(self):
        y_1 = self.position[1][0]
        y_1 -= 1
        self.position = (
            (self.position[0][0], self.position[0][1]),
            (y_1, self.position[1][1] + self.sprite.get_height()),
        )


def create_invaders(sprite, nb_inv):
    return [Invader(sprite) for _ in range(nb_inv)]


def create_entities(sprites):
    Entities = namedtuple("entities", ["player", "invaders", "missiles"])
    pla_sp, inv_sp, _ = sprites
    player = Player(pla_sp)
    invaders = create_invaders(inv_sp, NB_INVADERS)
    missiles = []
    return Entities(player=player, invaders=invaders, missiles=missiles)
