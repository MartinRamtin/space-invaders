# Space invaders

## Install

- install python
- create a virtual env: `python -m venv your/venv/name`
- activate your virtual env: `. you/venv/name`
- install pygame: `pip install pygame`

## Launch

./launch

## Default settings

left and right with arrows and fire with space

You can controls in the keybindings.txt file by writing :\
`left`\
`right`\
`up`\
`down`\
`space`\
`.`\
`'`\
`,`\
`;`\
`?`\
`!`\
or any `[a-z][0-9]`.
